package com.company;

class Complex {

        public double re, im;

        public Complex(double re, double im) {
                this.re = re;
                this.im = im;
        }

        public String toString() {
                return String.format(re + " + i" + im);
        }
        @Override
        public boolean equals(Object o) {


                if (o == this) {
                        return true;
                }


                if (!(o instanceof Complex)) {
                        return false;
                }


                Complex c = (Complex) o;


                return Double.compare(re, c.re) == 0
                        && Double.compare(im, c.im) == 0;
        }
        public static Complex sum(Complex c1, Complex c2)
        {
                //creating a temporary complex number to hold the sum of two numbers
                Complex temp = new Complex(0, 0);

                temp.re = c1.re + c2.re;
                temp.im = c1.im + c2.im;

                //returning the output complex number
                return temp;
        }
        




}

